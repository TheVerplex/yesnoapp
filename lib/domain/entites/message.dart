enum FromWho { mine, hers, me }

class Message {
  final String text;
  final String? imageUlr;
  final FromWho fromWho;

  Message(
      { //costructor//
      required this.text,
      this.imageUlr,
      required this.fromWho});
}
