import 'package:flutter/material.dart';
import 'package:yesnoapp/domain/entites/message.dart';

class ChatProvider extends ChangeNotifier {
  List<Message> messages = [
    Message(text: 'hola AMOR', fromWho: FromWho.me),
    Message(text: 'ya regrese', fromWho: FromWho.me),
  ];
//metodo para  enviar mensaje
  List<Message> get messageList => messages; // Corrección aquí

  Future<void> sendMessage(String text) async {
    // Implementa la lógica para enviar mensajes
  }
}
